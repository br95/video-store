package testviews;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import entities.City;
import entities.Country;

@Stateless
public class TestDao {

	@Inject 
	private EntityManager em;
	
	public Country getCountry(Integer id) {
		return em.find(Country.class, id);
	}
	public List<City> getCities(Country c) {
		Country country = em.find(Country.class, c.getCountryId());
		if(country != null) {
			List<City> cities = new ArrayList<>(country.getCities());
			c.setCities(cities);
			return cities;
		}
		else 
			return null;
	}
	
}
