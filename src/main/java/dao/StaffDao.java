package dao;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import entities.Staff;

@Stateless
public class StaffDao {
	
	@Inject
	private EntityManager entityManager;
	
	public Staff findStaffByUsernamePassword(String username,String password) {
		try {
			return entityManager.createQuery("SELECT s FROM Staff s WHERE s.username = :username AND s.password = :password",Staff.class)
					.setParameter("username", username)
					.setParameter("password", password)
					.getSingleResult();
		} catch(NoResultException e) {
			System.err.println(e.getLocalizedMessage());
			return null;
		}
	}
	
}
