package dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import entities.Actor;
import entities.Category;
import entities.Film;

@Stateless
public class FilmDao {

	@Inject
	private EntityManager entityManager;

	public List<Film> findAllFilms() {
		try {
			return entityManager.createNamedQuery("Film.findAll", Film.class).getResultList();
		} catch (NoResultException e) {
			System.err.println(e.getLocalizedMessage());
			return null;
		}
	}

	public Film findById(Integer filmId) {
		try {
			return entityManager.createQuery("SELECT f from Film f where f.filmId = :id", Film.class)
					.setParameter("id", filmId).getSingleResult();
		} catch (NoResultException e) {
			System.err.println(e.getLocalizedMessage());
			return null;
		}
	}

	public List<Film> allFilmsInStore(Integer storeId) {
		try {
			return entityManager.createQuery(
					"SELECT DISTINCT f from Film f " + "JOIN f.inventories i " + "where i.storeId = :storeId",
					Film.class).setParameter("storeId", storeId).getResultList();
		} catch (NoResultException e) {
			System.err.println(e.getLocalizedMessage());
			return null;
		}
	}
	
	public List<Actor> allFilmsActors(Film film) {
		Film f = entityManager.find(Film.class, film.getFilmId());
		if(f != null) {
			List<Actor> actors = new ArrayList<>(f.getActors());
			return actors;
 		}
		return null;
	}

	public List<Category> allFilmsCategories(Film film) {
		Film f = entityManager.find(Film.class, film.getFilmId());
		if (f != null) {
			List<Category> categories = new ArrayList<>(f.getCategories());
			return categories;
		} else
			return null;
	}

	public List<Category> allCategoriesInStore(Integer storeId) {
		try {
			return entityManager
					.createQuery("SELECT DISTINCT c from FilmCategory fc " + "JOIN fc.category c " + "JOIN fc.film f "
							+ "JOIN f.inventories i " + "WHERE i.storeId = :storeId", Category.class)
					.setParameter("storeId", storeId).getResultList();

		} catch (NoResultException e) {
			System.err.println(e.getLocalizedMessage());
			return null;
		}
	}

}
