package validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator(value = "logInValidator")
public class LogInValidator implements Validator<String> {

	@Override
	public void validate(FacesContext context, UIComponent component, String value) throws ValidatorException {
		String componentId = component.getClientId();
		if(componentId.equals("username")) {
			validateUsername(context, value);
		} else {
			validatePassword(context, value);
		}
	}
	
	private void validateUsername(FacesContext context, String username) {
		Matcher usernameMatch = Pattern.compile("[a-zA-Z0-9]{3,}").matcher(username);
		if(!usernameMatch.matches()) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR + " - Username must have at least 3 letters or numbers");
			throw new ValidatorException(message);
		}
	}

	private void validatePassword(FacesContext context, String password) {
		Matcher passwordMatch = Pattern.compile("[a-zA-Z0-9]{3,}").matcher(password);
		if(!passwordMatch.matches()) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR +  "- Password must have at least 8 letters or numbers");
			throw new ValidatorException(message);
		}
	}
}
