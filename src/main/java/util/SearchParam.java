package util;

public enum SearchParam {
	SEARCH_TEXT,
	RATING,
	RENTAL_RATE,
	DURATION
}
