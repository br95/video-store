package filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/*")
public class AuthorizationFilter implements Filter {

	private static final String LOGIN_PATH = "index.xhtml";
	private static final String MAIN_PATH = "maincontent.xhtml";
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		try {
			HttpServletRequest req = (HttpServletRequest) request;
			HttpServletResponse resp = (HttpServletResponse) response;
			HttpSession sess = req.getSession();
			String path = req.getRequestURI();
			boolean loggedIn = (sess != null && sess.getAttribute("username") != null);
			if (!path.contains("javax.faces.resource") 
					&& !path.contains("/VideoStore/"+LOGIN_PATH) 
					&& !loggedIn)
				resp.sendRedirect(LOGIN_PATH);
			else if(path.contains("/VideoStore/"+LOGIN_PATH) && loggedIn) 
				resp.sendRedirect(MAIN_PATH);
			else
				chain.doFilter(request, response);

		} catch (Exception e) {
			System.err.println(e.getLocalizedMessage());
		}
	}

}
