package views;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import controllers.FilmController;
import controllers.LogInController;
import entities.Film;
import util.SearchParam;

@Named("home")
@ViewScoped
public class HomeView implements Serializable {

	private static final long serialVersionUID = 546042753817157934L;

	@Inject
	private LogInController logInController;

	@Inject
	private FilmController filmController;

	private List<Film> filmsInStore;
	private Set<String> filmsRatings;
	private Set<String> filmsLanguages;
	private Set<String> allCategories;
	private Set<String> searchCategories;
	private List<String> selectedFilmActors;
	private Set<String> selectedFilmCategories;

	private String searchFilmValue;
	private String searchRating;
	private String searchRentalRate;
	private String searchDuration;

	private boolean renderFilmCategories = false;
	private boolean renderFilmActors = false;

	private Film selectedFilm;

	@PostConstruct
	public void initFilms() {
		Integer storeId = logInController.getLoggedStaff().getStoreId();
		this.filmsInStore = filmController.allFilmsInStore(storeId);
		this.filmsRatings = this.filmsInStore.stream().map(f -> f.getRating()).collect(Collectors.toSet());
		this.filmsLanguages = this.filmsInStore.stream().map(f -> f.getLanguage().getName())
				.collect(Collectors.toSet());
		this.allCategories = filmController.allCategoryNames(logInController.getLoggedStaff().getStoreId());
		this.searchRentalRate = "4.99";
		this.searchDuration = "300";
	}

	public void toggleRenderSelectedFilmData() {
		if (this.renderFilmActors)
			this.renderFilmActors = false;
		if (this.renderFilmCategories)
			this.renderFilmCategories = false;
	}

	public void onSelectFilmActors() {
		if (this.selectedFilm == null)
			return;
		this.selectedFilmActors = filmController.allFilmsActorsNames(this.selectedFilm);

		if (this.renderFilmActors)
			this.renderFilmActors = false;
		else
			this.renderFilmActors = true;
	}

	public void onSelectFilmCategories() {
		if (this.selectedFilm == null)
			return;

		this.selectedFilmCategories = filmController.allFilmsCategoryNames(this.selectedFilm);

		if (this.renderFilmCategories)
			this.renderFilmCategories = false;
		else
			this.renderFilmCategories = true;
	}

	public void onSearchFilms() {
		Map<SearchParam, String> searchParams = new HashMap<>();
		if (this.searchFilmValue == null || this.searchFilmValue.isBlank())
			searchParams.put(SearchParam.SEARCH_TEXT, null);
		else
			searchParams.put(SearchParam.SEARCH_TEXT, this.searchFilmValue);
		if (this.searchRating == null || this.searchRating.equals("dumy"))
			searchParams.put(SearchParam.RATING, null);
		else
			searchParams.put(SearchParam.RATING, this.searchRating);

		searchParams.put(SearchParam.RENTAL_RATE, this.searchRentalRate);
		searchParams.put(SearchParam.DURATION, this.searchDuration);

		Integer storeId = logInController.getLoggedStaff().getStoreId();
		if (this.searchCategories == null)
			this.filmsInStore = filmController.searchFilms(storeId, searchParams);
		else
			this.filmsInStore = filmController.searchFilms(storeId, searchParams, this.searchCategories);
	}

//

	public List<Film> getFilmsInStore() {
		return filmsInStore;
	}

	public boolean isRenderFilmCategories() {
		return renderFilmCategories;
	}

	public void setRenderFilmCategories(boolean renderFilmCategories) {
		this.renderFilmCategories = renderFilmCategories;
	}

	public boolean isRenderFilmActors() {
		return renderFilmActors;
	}

	public void setRenderFilmActors(boolean renderFilmActors) {
		this.renderFilmActors = renderFilmActors;
	}

	public List<String> getSelectedFilmActors() {
		return selectedFilmActors;
	}

	public void setSelectedFilmActors(List<String> selectedFilmActors) {
		this.selectedFilmActors = selectedFilmActors;
	}

	public Set<String> getSelectedFilmCategories() {
		return selectedFilmCategories;
	}

	public void setSelectedFilmCategories(Set<String> selectedFilmCategories) {
		this.selectedFilmCategories = selectedFilmCategories;
	}

	public Film getSelectedFilm() {
		return selectedFilm;
	}

	public void setSelectedFilm(Film selectedFilm) {
		this.selectedFilm = selectedFilm;
	}

	public Set<String> getFilmsCategories() {
		return allCategories;
	}

	public void setFilmsCategories(Set<String> allCategories) {
		this.allCategories = allCategories;
	}

	public Set<String> getSearchCategories() {
		return searchCategories;
	}

	public void setSearchCategories(Set<String> searchCategories) {
		this.searchCategories = searchCategories;
	}

	public String getSearchDuration() {
		return searchDuration;
	}

	public void setSearchDuration(String searchDuration) {
		this.searchDuration = searchDuration;
	}

	public String getSearchRentalRate() {
		return searchRentalRate;
	}

	public void setSearchRentalRate(String searchRentalRate) {
		this.searchRentalRate = searchRentalRate;
	}

	public Set<String> getFilmsLanguages() {
		return filmsLanguages;
	}

	public void setFilmsLanguages(Set<String> filmsLanguages) {
		this.filmsLanguages = filmsLanguages;
	}

	public void setFilmsInStore(List<Film> filmsInStore) {
		this.filmsInStore = filmsInStore;
	}

	public String getSearchFilmValue() {
		return searchFilmValue;
	}

	public void setSearchFilmValue(String searchFilmValue) {
		this.searchFilmValue = searchFilmValue;
	}

	public String getSearchRating() {
		return searchRating;
	}

	public void setSearchRating(String searchRating) {
		this.searchRating = searchRating;
	}

	public Set<String> getFilmsRatings() {
		return filmsRatings;
	}

	public void setFilmsRatings(Set<String> filmsRatings) {
		this.filmsRatings = filmsRatings;
	}

}
