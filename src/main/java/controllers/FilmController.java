package controllers;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import dao.FilmDao;
import entities.Film;
import util.SearchParam;

@RequestScoped
public class FilmController implements Serializable {

	private static final long serialVersionUID = 6618202599100678668L;

	@Inject 
	private FilmDao filmDao;

	public List<Film> allFilmsInStore(Integer storeId) {
		return filmDao.allFilmsInStore(storeId);
	}

	public List<Film> allFilms() {
		return filmDao.findAllFilms();
	}

	public Set<String> allCategoryNames(Integer storeId) {
		return filmDao
				.allCategoriesInStore(storeId)
				.stream()
				.map(c -> c.getName())
				.collect(Collectors.toSet());
	}
	

	public Set<String> allFilmsCategoryNames(Film film) {
		return filmDao.allFilmsCategories(film)
				.stream()
				.map(c -> c.getName())
				.collect(Collectors.toSet());
	}
	
	public List<String> allFilmsActorsNames(Film film) {
		return filmDao.allFilmsActors(film)
				.stream()
				.map(a -> a.getFirstName() + " " + a.getLastName())
				.collect(Collectors.toList());
	}
	
	

	public List<Film> searchFilms(Integer storeId, Map<SearchParam, String> searchParams) {
		List<Film> res = filmDao.allFilmsInStore(storeId);
		for (Entry<SearchParam, String> paramEntry : searchParams.entrySet()) {
			if (paramEntry.getValue() == null)
				continue;
			switch (paramEntry.getKey()) {
			case SEARCH_TEXT:
				res = res.stream()
						.filter(f -> f.getTitle().toLowerCase().contains(paramEntry.getValue().toLowerCase())
								|| f.getDescription().toLowerCase().contains(paramEntry.getValue().toLowerCase()))
						.collect(Collectors.toList());
				break;
			case RATING:
				res = res.stream().filter(f -> f.getRating().equals(paramEntry.getValue()))
						.collect(Collectors.toList());
				break;
			case RENTAL_RATE:
				res = res.stream().filter(f -> f.getRentalRate().doubleValue() <= Double.valueOf(paramEntry.getValue()))
						.collect(Collectors.toList());
				break;
			case DURATION:
				res = res.stream().filter(f -> f.getLength() <= Integer.valueOf(paramEntry.getValue()))
						.collect(Collectors.toList());
				break;
			default:
				break;
			}
		}
		return res;
	}

	public List<Film> searchFilms(Integer storeId, Map<SearchParam, String> searchParams, Set<String> searchCategories) {
		return searchFilms(storeId, searchParams)
				.stream()
				.filter(f -> allFilmsCategoryNames(f).containsAll(searchCategories))
				.collect(Collectors.toList());
		
	}

//

}
