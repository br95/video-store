package controllers;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Named;

@Named("content")
@SessionScoped
public class ContentController implements Serializable {

	private static final long serialVersionUID = -4462125579878433581L;

	private boolean home = true;
	private boolean customers = false;
	private boolean inventory = false;
	private boolean payments = false;
	private boolean rental = false;
	private boolean staffData = false;
	private boolean store = false;
	private boolean cart = false;

//
//	Sets only the selected field/content to be true for rendering
	public void loadContent(ActionEvent e) {
		List<Field> allFields = new ArrayList<>(Arrays.asList(this.getClass().getDeclaredFields()));
		for (Field f : allFields) {
			try {
				if(f.getName().equals("serialVersionUID")) continue;
				if (f.getName().equals(e.getComponent().getId())) {
					f.set(this, true);
				} else
					f.set(this, false);

			} catch (IllegalArgumentException e1) {
				System.err.println(e1.getLocalizedMessage());
			} catch (IllegalAccessException e1) {
				System.err.println(e1.getLocalizedMessage());
			}
		}
	}

//
	
	

	public void goInventory() {
		this.inventory = true;
	}

	public boolean isCart() {
		return cart;
	}

	public void setCart(boolean cart) {
		this.cart = cart;
	}

	public boolean isHome() {
		return home;
	}

	public void setHome(boolean home) {
		this.home = home;
	}

	public boolean isCustomers() {
		return customers;
	}

	public void setCustomers(boolean customers) {
		this.customers = customers;
	}

	public boolean isPayments() {
		return payments;
	}

	public void setPayments(boolean payments) {
		this.payments = payments;
	}

	public boolean isRental() {
		return rental;
	}

	public void setRental(boolean rental) {
		this.rental = rental;
	}

	public boolean isStaffData() {
		return staffData;
	}

	public void setStaffData(boolean staffData) {
		this.staffData = staffData;
	}

	public boolean isStore() {
		return store;
	}

	public void setStore(boolean store) {
		this.store = store;
	}

	public boolean isCustomer() {
		return customers;
	}

	public void setCustomer(boolean customers) {
		this.customers = customers;
	}

	public boolean isInventory() {
		return inventory;
	}

	public void setInventory(boolean inventory) {
		this.inventory = inventory;
	}

}
