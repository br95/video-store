package controllers;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import dao.StaffDao;
import entities.Staff;
import util.SessionUtil;

@Named("logIn")
@SessionScoped
public class LogInController implements Serializable {

	private static final long serialVersionUID = 5489869140807250038L;

	private static final String MAIN_CONTENT = "maincontent";
	private static final String LOG_IN = "index";
	
	@Inject
	private StaffDao staffDao;

	private String username;
	private String password;

	private Staff loggedStaff;

	public String logInUser() {
		loggedStaff = staffDao.findStaffByUsernamePassword(this.username, this.password);
		if (loggedStaff == null) {
			FacesMessage message = new FacesMessage(
					"Username or password doesn't match any Staff credentials, try again!");
			FacesContext.getCurrentInstance().addMessage("growl-login", message);
			return LOG_IN;
		} else {
			HttpSession session = SessionUtil.getSession();
			session.setAttribute("username",this.username);
			return MAIN_CONTENT;
		}
	}

	public String logOutUser() {
		HttpSession session = SessionUtil.getSession();
		session.invalidate();
		return LOG_IN;
	}
// 

	public void setUsername(String username) {
		this.username = username;
	}

	public Staff getLoggedStaff() {
		return loggedStaff;
	}

	public void setLoggedStaff(Staff loggedStaff) {
		this.loggedStaff = loggedStaff;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
