//package services;
//
//import java.io.StringWriter;
//import java.util.List;
//
//import javax.inject.Inject;
//import javax.json.Json;
//import javax.json.JsonArrayBuilder;
//import javax.json.JsonObject;
//import javax.json.JsonObjectBuilder;
//import javax.json.JsonWriter;
//import javax.ws.rs.GET;
//import javax.ws.rs.Path;
//import javax.ws.rs.PathParam;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.MediaType;
//
//import dao.FilmDao;
//import entities.Film;
//
//@Path("/films")
//public class FilmService {
//
//	@Inject
//	private FilmDao filmDao;
//
//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	public String getFilms() {
//		List<Film> films = filmDao.getAllFilms();
//		JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
//		JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
//		for (Film f : films) {
//			jsonArrayBuilder.add(jsonBuilder.add("film_id", f.getFilmId()).add("title", f.getTitle())
//					.add("description", f.getDescription()).add("length", f.getLength())
//					.add("language", f.getLanguage().getName().trim()).add("rating", f.getRating()).build());
//		}
//		StringWriter stringWriter = new StringWriter();
//		try (JsonWriter jsonWriter = Json.createWriter(stringWriter)) {
//			jsonWriter.writeArray(jsonArrayBuilder.build());
//		}
//		return stringWriter.toString();
//	}
//
//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	@Path("/{filmId}")
//	public String getFilm(@PathParam("filmId") Integer filmId) {
//		Film film = filmDao.getFilm(filmId);
//		JsonObject json = Json.createObjectBuilder().add("film_id", film.getFilmId()).add("title", film.getTitle())
//				.add("length", film.getLength()).add("rating", film.getRating()).build();
//		StringWriter stringWriter = new StringWriter();
//		try (JsonWriter jsonWriter = Json.createWriter(stringWriter)) {
//			jsonWriter.writeObject(json);
//		}
//		return stringWriter.toString();
//	}
//}
