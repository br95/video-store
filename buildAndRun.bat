@echo off
call mvn clean package
call docker build -t me.coreit/projekat2 .
call docker rm -f projekat2
call docker run -d -p 8080:8080 -p 4848:4848 --name projekat2 me.coreit/projekat2